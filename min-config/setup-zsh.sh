# install zsh, nvim
sudo apt-get install -y zsh curl

# install pureprompt
[ -d $HOME/.zsh ] || mkdir -p "$HOME/.zsh"
git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"

curl -L git.io/antigen > ~/.zsh/antigen.zsh

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh/zsh-syntax-highlighting

. ~/.zshrc
