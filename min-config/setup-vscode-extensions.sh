code --install-extension GitHub.github-vscode-theme
code --install-extension vscode-icons-team.vscode-icons
code --install-extension VisualStudioExptTeam.vscodeintellicode
code --install-extension stkb.rewrap
code --install-extension esbenp.prettier-vscode
code --install-extension dbaeumer.vscode-eslint
code --install-extension eamodio.gitlens

code --install-extension ms-vscode-remote.remote-containers
code --install-extension ms-vscode-remote.remote-ssh
code --install-extension ms-vscode-remote.remote-wsl
code --install-extension ms-azuretools.vscode-docker

code --install-extension vscodevim.vim
code --install-extension DavidAnson.vscode-markdownlint
code --install-extension exiasr.hadolint
